package com.adpro.adproservice.model;

import java.util.Arrays;

public class User {

    private int id;
    private String firstName;
    private String lastName;
    private String emailId;
    private String password;
    private Site[] sites;

    public User() {
    }


    public User(int id, String firstName, String lastName, String emailId, String password, Site[] sites) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailId = emailId;
        this.password = password;
        this.sites = sites;
    }

    public User(String firstName, String lastName, String emailId, String password, Site[] sites) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailId = emailId;
        this.password = password;
        this.sites = sites;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Site[] getSites() {
        return sites;
    }

    public void setSites(Site[] sites) {
        this.sites = sites;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", emailId='" + emailId + '\'' +
                ", password='" + password + '\'' +
                ", sites=" + Arrays.toString(sites) +
                '}';
    }

    @Override
    public int hashCode(){
        return id;
    }

    @Override
    public boolean equals(Object obj){
        if(!(obj instanceof User)) return false;
        User user = (User) obj;
        return this.getId() == user.getId() && Arrays.equals(this.sites, user.sites);
    }
}
