package com.adpro.adproservice.model;


public class Site {

    private String id;
    private String siteUrl;
    private int userId;

    private Site() {
    }

    public Site(String id, String siteUrl, int userId) {
        this.id = id;
        this.siteUrl = siteUrl;
        this.userId = userId;
    }

    public Site(String siteUrl, int userId)
    {
        this.siteUrl = siteUrl;
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSiteUrl() {
        return siteUrl;
    }

    public void setSiteUrl(String siteUrl) {
        this.siteUrl = siteUrl;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Site{" +
                "id='" + id + '\'' +
                ", siteUrl='" + siteUrl + '\'' +
                ", userId=" + userId +
                '}';
    }

    @Override
    public int hashCode(){
        return userId;
    }

    @Override
    public boolean equals(Object obj){
        if(!(obj instanceof Site))  return false;
        Site site = (Site) obj;
        return id.equals(site.getId()) && siteUrl.equals(site.getSiteUrl());
    }
}
