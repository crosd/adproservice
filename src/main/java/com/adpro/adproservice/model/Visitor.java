package com.adpro.adproservice.model;


public class Visitor {

    private int id;
    private String siteId;
    private String ipAddress;
    private String userAgent;
    private long time;
    private double latitude;
    private double longitude;

    public Visitor() {
    }

    public Visitor(int id, String siteId, String ipAddress, String userAgent, long time, double latitude, double longitude) {
        this.id = id;
        this.siteId = siteId;
        this.ipAddress = ipAddress;
        this.userAgent = userAgent;
        this.time = time;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Visitor(String siteId, String ipAddress, String userAgent, long time, double latitude, double longitude) {
        this.siteId = siteId;
        this.ipAddress = ipAddress;
        this.userAgent = userAgent;
        this.time = time;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Visitor(String siteId, String ipAddress, String userAgent, long time) {
        this.siteId = siteId;
        this.ipAddress = ipAddress;
        this.userAgent = userAgent;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "Visitor{" +
                "id=" + id +
                ", siteId='" + siteId + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                ", userAgent='" + userAgent + '\'' +
                ", time=" + time +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Visitor)) return false;
        Visitor visitor = (Visitor) obj;
        return id == visitor.getId()
                && siteId == visitor.getSiteId()
                && ipAddress == visitor.getIpAddress();
    }

    @Override
    public int hashCode() {
        return id;
    }
}
