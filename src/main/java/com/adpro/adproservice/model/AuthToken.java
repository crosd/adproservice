package com.adpro.adproservice.model;

import java.util.Date;


public class AuthToken {
    private String token;
    private String emailId;
    private long start;
    private long lastActive;

    public AuthToken() {
    }

    public AuthToken(String token, String emailId, long start, long lastActive) {
        this.token = token;
        this.emailId = emailId;
        this.start = start;
        this.lastActive = lastActive;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public void setToken(String token) {

        this.token = token;
    }

    public String getToken() {

        return token;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getLastActive() {
        return lastActive;
    }

    public void setLastActive(long lastActive) {
        this.lastActive = lastActive;
    }

    @Override
    public String toString() {
        return "AuthToken{" +
                " encoded token=" + token +
                ", emailId=" + emailId +
                '}';
    }
}
