package com.adpro.adproservice.testmain;

import com.adpro.adproservice.model.Site;
import com.adpro.adproservice.model.User;
import com.adpro.adproservice.services.UserService;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;

public class TestUser {

    public static void main(String[] args) {


//        test for base64 encoding
//        String userEmail = "email@gmail.com";
//        String psw = "password";
//        double token = Math.random();     //store this token in db
//        String str = userEmail + ":" + psw + String.valueOf(token);
//        try {
//            String encoding = Base64.getUrlEncoder().encodeToString(str.getBytes("utf-8"));
//            System.out.println(encoding);
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }


//        test for addUser()
//        User user = new User();
//        Site site = null;
//        user.setFirstName("Bill");
//        user.setLastName("Gates");
//        user.setEmailId("gates@outlook.com");
//        user.setPassword("gates bill");
//        URL url = null;
//        try {
//            url = new URL("http://www.microsoft.com");
//            site = new Site(url);
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }
//        user.setSite(site);
//        new UserService().addUser(user);



//        test for getUser(emailId)
//        System.out.println(new UserService().getUser("johndoe@gmail.com"));

//        test for getUser(userId)
//        System.out.println(new UserService().getUser(11));


//        test for getAllUsers()
//        System.out.println(new UserService().getAllUsers());


//        test for updateUser()
//        URL url = null;
//        try {
//            url = new URL("http://www.apple.com");
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }
//        System.out.println(new UserService().updateUser(new User("Steve", "Wozniak", "steve@apple.com", "woz steve", new Site(url))));



//        test for deleteUser()
//        UserService userService = new UserService();
//        System.out.println(userService.removeUser(userService.getUser(1)));
    }

}
