package com.adpro.adproservice.testmain;

import com.adpro.adproservice.dao.VisitorDao;
import com.adpro.adproservice.model.Visitor;
import com.adpro.adproservice.services.VisitorService;
import com.maxmind.geoip.Location;
import com.maxmind.geoip.LookupService;

import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

public class VisitorTest {
    public static void main(String[] args) {
        VisitorService visitorService = new VisitorService();

        String ipAddress = "27.34.127.156";
        String userAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";
        String siteId1 = "4f1aeecd-10f7-4745-ad00-2ef35441846f";
        String siteId2 = "83c02638-bdb5-4198-811f-0d6b06ab3609";

//          test for addVisitor()
//        Visitor visitor = new Visitor(siteId2, ipAddress, userAgent, new Date().getTime(), location.latitude, location.longitude);
//        Visitor visitor = new Visitor("3ff38390-07bd-451d-a8ec-814ab9d52ad8", "27.34.65.234", userAgent, new Date().getTime());
//        System.out.println(visitorService.addVisitor(visitor));

//        test for getVisitorsByIp()
//        System.out.println(visitorService.getVisitorsByIp(ipAddress));

//        test for getVisitorsBySiteId()
//        System.out.println(visitorService.getVisitorsBySiteId(siteId2));

//        test for getAllVisitors()
//        System.out.println(visitorService.getAllVisitors());

//       test for getVisitor() by siteId and ipAddress
//        System.out.println(visitorService.getVisitor("3ff38390-07bd-451d-a8ec-814ab9d52ad8", "27.34.65.234"));

//        test for updateVisitor()
//        visitorService.updateVisitor(new Visitor(siteId2, ipAddress, userAgent, new Date().getTime(), location.latitude, location.longitude));

//        test for removeVisitor()
//        visitorService.removeVisitor(visitorService.getVisitorsByIp(ipAddress).get(1));

    }
}
