package com.adpro.adproservice.services;

import com.adpro.adproservice.dao.SessionDao;
import com.adpro.adproservice.model.AuthToken;
import com.adpro.adproservice.model.User;

import java.util.concurrent.TimeUnit;


public class SessionService {
    private SessionDao sessionDao = new SessionDao();

    public AuthToken sessionStart(User user) {
        return sessionDao.sessionStart(user);
    }

    public boolean sessionEnd(AuthToken authToken) {
        return sessionDao.sessionEnd(authToken);
    }

    public String encodeAuthToken(String email) {
        return sessionDao.encodeAuthToken(email);
    }

    public AuthToken generateAuthToken(User user) {
        return sessionDao.generateAuthToken(user);
    }

    public AuthToken getAuthToken(String email) {
        return sessionDao.getAuthToken(email);
    }

    public boolean validateToken(AuthToken authToken) {
        AuthToken validAuthtoken = this.getAuthToken(authToken.getEmailId());
        long diff = authToken.getLastActive()- validAuthtoken.getLastActive();
        long diffMinutes = TimeUnit.MILLISECONDS.toMinutes(diff); //session expire calculation in minutes

        String validToken = sessionDao.decodeAuthToken(validAuthtoken);
        String userToken = sessionDao.decodeAuthToken(authToken);

        return (validToken.equals(userToken) && diffMinutes < 5);
    }

    public boolean setLastActive(AuthToken authToken) { //for updating ActiveLast

        return sessionDao.setLastActive(authToken);
    }

    public String getHeaderAuthToken(String autheader) { //for getting header token
        String[] elements;
        String scheme;
        if (!autheader.isEmpty()) {
            elements = autheader.split(" ");
            if (elements.length == 2) {
                scheme = elements[0];
                if (scheme.equals("Bearer")) {
                    return elements[1];
                }
            }
        }
        return null;
    }

    public void sessionFlush() {// for flushing inactive sessions
         sessionDao.sesssionFlush();
    }
}
