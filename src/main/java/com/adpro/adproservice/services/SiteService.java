package com.adpro.adproservice.services;

import com.adpro.adproservice.dao.SiteDao;
import com.adpro.adproservice.model.Site;

import java.util.List;

public class SiteService {
    
    SiteDao siteDao = new SiteDao();

    public boolean addSite(Site site) {
        return siteDao.createSite(site);
    }

    public List<Site> getAllSites(int userId) {
        return siteDao.retrieveAllSites(userId);
    }

    public List<Site> getAllSites() {
        return siteDao.retrieveAllSites();
    }

    public boolean updateSite(Site site) {
        return siteDao.updateSite(site);
    }

    public boolean removeSite(String siteId) {
        return siteDao.deleteSite(siteId);
    }

}
