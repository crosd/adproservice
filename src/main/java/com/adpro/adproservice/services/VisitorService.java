package com.adpro.adproservice.services;

import com.adpro.adproservice.dao.VisitorDao;
import com.adpro.adproservice.model.Visitor;
import com.maxmind.geoip.Location;
import com.maxmind.geoip.LookupService;

import java.util.Iterator;
import java.util.List;

public class VisitorService {

    VisitorDao visitorDao = new VisitorDao();

    public boolean addVisitor(Visitor visitor) {
//        check if visitor already exists
        Visitor v = getVisitor(visitor.getSiteId(), visitor.getIpAddress());
        if (v != null) return false;

//        set the longitude and latitude if its not set
        if (visitor.getLongitude() == 0.0 || visitor.getLongitude() == 0.0) {
            LookupService lookupService;
            try {
                lookupService = new LookupService("C:\\Users\\Windows7\\IdeaProjects\\adproservice\\GeoLiteCity.dat",
                        LookupService.GEOIP_MEMORY_CACHE | LookupService.GEOIP_CHECK_CACHE);
                Location location = lookupService.getLocation(visitor.getIpAddress());
                visitor.setLatitude(location.latitude);
                visitor.setLongitude(location.longitude);
            } catch (Exception ex) {
                System.out.println("Exception : " + ex);
            }
        }
        return visitorDao.createVisitor(visitor);
    }

    public List<Visitor> getVisitorsByIp(String ipAddress) {
        return visitorDao.retrieveVisitorsByIp(ipAddress);
    }

    public List<Visitor> getVisitorsBySiteId(String siteId) {
        return visitorDao.retrieveVisitorsBySiteId(siteId);
    }

    public Visitor getVisitor(String siteId, String ipAddress) {
        return visitorDao.retrieveVisitor(siteId, ipAddress);
    }

    public List<Visitor> getAllVisitors() {
        return visitorDao.retrieveAllVisitors();
    }

    public boolean updateVisitor(Visitor visitor) {
        return visitorDao.updateVisitor(visitor);
    }

    public boolean removeVisitor(Visitor visitor) {
        return visitorDao.deleteVisitor(visitor);
    }

}
