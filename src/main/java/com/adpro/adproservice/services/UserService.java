package com.adpro.adproservice.services;

import com.adpro.adproservice.dao.UserDao;
import com.adpro.adproservice.dbconnection.ConnectionPool;
import com.adpro.adproservice.exceptionhandling.SQLExceptionHandler;
import com.adpro.adproservice.model.User;

import java.net.URL;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserService {

    private UserDao userDao = new UserDao();

    public boolean addUser(User user) {
        return userDao.createUser(user);
    }

    public User getUser(String emailId) {
        return userDao.retrieveUser(emailId);
    }

    public User getUser(int userId) {
        return userDao.retrieveUser(userId);
    }

    public List<User> getAllUsers() {
        return userDao.retrieveAllUsers();
    }

    public boolean updateUser(User user) {
        return userDao.updateUser(user);
    }

    public boolean removeUser(int userId) {
        return userDao.deleteUser(userId);
    }

    public boolean validateUser(User user) {
        User validUser = this.getUser(user.getEmailId());
        return validUser.getPassword().equals(user.getPassword());
    }

}
