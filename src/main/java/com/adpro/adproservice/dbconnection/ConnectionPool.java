package com.adpro.adproservice.dbconnection;

import org.apache.commons.dbcp2.BasicDataSource;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;

public class ConnectionPool {

    private static ConnectionPool connectionPool;
    private BasicDataSource basicDataSource;

    private ConnectionPool() throws IOException, SQLException, PropertyVetoException {
        basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName("com.mysql.jdbc.Driver");
        basicDataSource.setUsername("root");
        basicDataSource.setPassword("");
        basicDataSource.setUrl("jdbc:mysql://localhost:3306/adpro");

        // the settings below are optional -- dbcp can work with defaults
        basicDataSource.setMinIdle(5);
        basicDataSource.setMaxIdle(20);
        basicDataSource.setMaxOpenPreparedStatements(180);

    }

    public static ConnectionPool getInstance() throws IOException, SQLException, PropertyVetoException {
        if (connectionPool == null) {
            connectionPool = new ConnectionPool();
            return connectionPool;
        } else {
            return connectionPool;
        }
    }

    public Connection getConnection() throws SQLException {
        return this.basicDataSource.getConnection();
    }

    public void shutdownPool() throws SQLException {
        if (this.basicDataSource != null)
            this.basicDataSource.close();
    }

}