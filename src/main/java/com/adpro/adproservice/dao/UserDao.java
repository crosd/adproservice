package com.adpro.adproservice.dao;

import com.adpro.adproservice.dbconnection.ConnectionPool;
import com.adpro.adproservice.exceptionhandling.SQLExceptionHandler;
import com.adpro.adproservice.model.Site;
import com.adpro.adproservice.model.User;

import java.net.URL;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserDao {

    private Connection con = null;
    private PreparedStatement preparedStatement = null;
    private PreparedStatement sitePreparedStatement = null;
    private Statement statement = null;
    private ResultSet resultSet = null;

    public boolean createUser(User user) {
        if (user == null) return false;

        try {
            con = ConnectionPool.getInstance().getConnection();
            con.setAutoCommit(false);

            preparedStatement = con.prepareStatement("INSERT INTO user VALUES(DEFAULT,?,?,?,?)");
            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getEmailId());
            preparedStatement.setString(4, user.getPassword());

            int userRowsAffected = preparedStatement.executeUpdate();

            try (PreparedStatement ps = con.prepareStatement("SELECT id FROM user WHERE email_id = ?")) {
                ps.setString(1, user.getEmailId());
                resultSet = ps.executeQuery();
                resultSet.next();
                user.setId(resultSet.getInt("id"));
            }

            sitePreparedStatement = con.prepareStatement("INSERT INTO site VALUES(?,?,?)");
            sitePreparedStatement.setString(1, UUID.randomUUID().toString());
            sitePreparedStatement.setString(2, user.getSites()[0].getSiteUrl());
            sitePreparedStatement.setInt(3, user.getId());

            int siteRowsAffected = sitePreparedStatement.executeUpdate();

            con.commit();
            if (userRowsAffected == 1 && siteRowsAffected == 1) {
                return true;
            }

        } catch (SQLException e) {
            SQLExceptionHandler.printSQLException(e);

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                if (resultSet != null) resultSet.close();
                if (preparedStatement != null) preparedStatement.close();
                if (sitePreparedStatement != null) sitePreparedStatement.close();
                if (con != null) con.close();
            } catch (SQLException e) {
                SQLExceptionHandler.printSQLException(e);
            }
        }
        return false;
    }

    public User retrieveUser(String emailId) {
        if (emailId == null) return null;
        try {
            con = ConnectionPool.getInstance().getConnection();
            con.setAutoCommit(false);

            String query = "SELECT id, first_name, last_name, password FROM user WHERE email_id = ?";
            preparedStatement = con.prepareStatement(query);
            preparedStatement.setString(1, emailId);

            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {

                int userId = resultSet.getInt("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String password = resultSet.getString("password");

                query = "SELECT id, site_url FROM site WHERE user_id = ?";
                sitePreparedStatement = con.prepareStatement(query);
                sitePreparedStatement.setInt(1, userId);

                resultSet = sitePreparedStatement.executeQuery();
                con.commit();

                List<Site> sites = new ArrayList<>();
                while (resultSet.next()) {

                    String siteId = resultSet.getString("id");
                    String siteUrl = resultSet.getString("site_url");
                    sites.add(new Site(siteId, siteUrl, userId));
                }
                Site siteArray[] = new Site[sites.size()];
                siteArray = sites.toArray(siteArray);
                return new User(userId, firstName, lastName, emailId, password, siteArray);
            }

        } catch (SQLException e) {
            SQLExceptionHandler.printSQLException(e);

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                if (resultSet != null) resultSet.close();
                if (preparedStatement != null) preparedStatement.close();
                if (con != null) con.close();
            } catch (SQLException e) {
                SQLExceptionHandler.printSQLException(e);
            }
        }
        return null;
    }

    public User retrieveUser(int userId) {
        if (userId < 1) return null;
        try {
            con = ConnectionPool.getInstance().getConnection();
            con.setAutoCommit(false);

            String query = "SELECT id, first_name, last_name, email_id, password FROM user WHERE id = ?";
            preparedStatement = con.prepareStatement(query);
            preparedStatement.setInt(1, userId);

            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {

                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String emailId = resultSet.getString("email_id");
                String password = resultSet.getString("password");

                query = "SELECT id, site_url FROM site WHERE user_id = ?";
                sitePreparedStatement = con.prepareStatement(query);
                sitePreparedStatement.setInt(1, userId);

                resultSet = sitePreparedStatement.executeQuery();
                con.commit();

                List<Site> sites = new ArrayList<>();
                while (resultSet.next()) {

                    String siteId = resultSet.getString("id");
                    String siteUrl = resultSet.getString("site_url");
                    sites.add(new Site(siteId, siteUrl, userId));
                }
                Site siteArray[] = new Site[sites.size()];
                siteArray = sites.toArray(siteArray);
                return new User(userId, firstName, lastName, emailId, password, siteArray);
            }

        } catch (SQLException e) {
            SQLExceptionHandler.printSQLException(e);

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                if (resultSet != null) resultSet.close();
                if (preparedStatement != null) preparedStatement.close();
                if (con != null) con.close();
            } catch (SQLException e) {
                SQLExceptionHandler.printSQLException(e);
            }
        }
        return null;
    }

    public List<User> retrieveAllUsers() {

        List<User> users = new ArrayList<>();
        try {
            con = ConnectionPool.getInstance().getConnection();
            con.setAutoCommit(false);
            String query = "SELECT id, first_name, last_name, email_id, password FROM user";
            statement = con.createStatement();
            resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                int userId = resultSet.getInt("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String emailId = resultSet.getString("email_id");
                String password = resultSet.getString("password");

                query = "SELECT id, site_url FROM site WHERE user_id = ?";
                sitePreparedStatement = con.prepareStatement(query);
                sitePreparedStatement.setInt(1, userId);
                try (ResultSet siteResultSet = sitePreparedStatement.executeQuery()) {
                    con.commit();

                    List<Site> sites = new ArrayList<>();
                    while (siteResultSet.next()) {
                        String siteId = siteResultSet.getString("id");
                        String siteUrl = siteResultSet.getString("site_url");
                        sites.add(new Site(siteId, siteUrl, userId));
                    }
                    Site siteArray[] = new Site[sites.size()];
                    siteArray = sites.toArray(siteArray);
                    users.add(new User(userId, firstName, lastName, emailId, password, siteArray));
                }
            }
            return users;
        } catch (SQLException e) {
            SQLExceptionHandler.printSQLException(e);
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                if (resultSet != null) resultSet.close();
                if (statement != null) statement.close();
                if (con != null) con.close();
            } catch (SQLException e) {
                SQLExceptionHandler.printSQLException(e);
            }
        }
        return null;
    }

    public boolean updateUser(User user) {
        if (user == null) return false;
        try {
            con = ConnectionPool.getInstance().getConnection();
            con.setAutoCommit(false);

            String userQuery = "UPDATE user SET first_name = ?, last_name=?, password=? WHERE email_id=?";
            preparedStatement = con.prepareStatement(userQuery);
            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getPassword());
            preparedStatement.setString(4, user.getEmailId());

//            try (PreparedStatement ps = con.prepareStatement("SELECT id FROM user WHERE email_id = ?")) {
//                ps.setString(1, user.getEmailId());
//                resultSet = ps.executeQuery();
//                resultSet.next();
//                user.setId(resultSet.getInt("id"));
//            }
//
//            String siteQuery = "UPDATE site SET site_url = ? WHERE user_id = ?";
//            sitePreparedStatement = con.prepareStatement(siteQuery);
//            sitePreparedStatement.setString(1, user.getSiteUrl());
//            sitePreparedStatement.setInt(2, user.getId());
//
            int userRowsAffected = preparedStatement.executeUpdate();
//            int siteRowsAffected = sitePreparedStatement.executeUpdate();

            con.commit();

//            if (userRowsAffected == 1 && siteRowsAffected >= 1) return true;

            if (userRowsAffected == 1) return true;

        } catch (SQLException e) {
            SQLExceptionHandler.printSQLException(e);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) preparedStatement.close();
                if (con != null) con.close();
            } catch (SQLException e) {
                SQLExceptionHandler.printSQLException(e);
            }
        }
        return false;
    }

    public boolean deleteUser(int id) {
        if (id < 1) return false;
        try {
            con = ConnectionPool.getInstance().getConnection();

            String query = "DELETE FROM user WHERE id = ?";

            preparedStatement = con.prepareStatement(query);
            preparedStatement.setInt(1, id);

            if (preparedStatement.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException e) {
            SQLExceptionHandler.printSQLException(e);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) preparedStatement.close();
                if (con != null) con.close();
            } catch (SQLException e) {
                SQLExceptionHandler.printSQLException(e);
            }
        }
        return false;
    }

}