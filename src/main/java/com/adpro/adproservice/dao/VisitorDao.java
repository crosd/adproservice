package com.adpro.adproservice.dao;

import com.adpro.adproservice.dbconnection.ConnectionPool;
import com.adpro.adproservice.exceptionhandling.SQLExceptionHandler;
import com.adpro.adproservice.model.Visitor;

import java.math.BigDecimal;
import java.net.URL;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class VisitorDao {

    private Connection con = null;
    private PreparedStatement preparedStatement = null;
    private Statement statement = null;
    private ResultSet resultSet = null;

    public boolean createVisitor(Visitor visitor) {
        if (visitor == null) return false;
        try {
            con = ConnectionPool.getInstance().getConnection();

            preparedStatement = con.prepareStatement("INSERT INTO visitor VALUES(DEFAULT,?,?,?,?,?,?)");
            preparedStatement.setString(1, visitor.getSiteId());
            preparedStatement.setString(2, visitor.getIpAddress());
            preparedStatement.setString(3, visitor.getUserAgent());
            preparedStatement.setLong(4, visitor.getTime());
            preparedStatement.setDouble(5, visitor.getLatitude());
            preparedStatement.setDouble(6, visitor.getLongitude());

            if (preparedStatement.executeUpdate() == 1) return true;

        } catch (SQLException e) {
            SQLExceptionHandler.printSQLException(e);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) preparedStatement.close();
                if (con != null) con.close();
            } catch (SQLException e) {
                SQLExceptionHandler.printSQLException(e);
            }
        }
        return false;
    }

    public List<Visitor> retrieveVisitorsByIp(String ipAddress) {
        if (ipAddress == null) return null;

        List<Visitor> visitors = new ArrayList<>();
        try {
            con = ConnectionPool.getInstance().getConnection();

            String query = "SELECT id, site_id, user_agent, time, latitude, longitude FROM visitor WHERE ip_address = ?";
            preparedStatement = con.prepareStatement(query);
            preparedStatement.setString(1, ipAddress);

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                int id = resultSet.getInt("id");
                String siteId = resultSet.getString("site_id");
                String userAgent = resultSet.getString("user_agent");
                Long time = resultSet.getLong("time");
                double latitude = resultSet.getDouble("latitude");
                double longitude = resultSet.getDouble("longitude");

                visitors.add(new Visitor(id, siteId, ipAddress, userAgent, time, latitude, longitude));
            }
            return visitors;
        } catch (SQLException e) {
            SQLExceptionHandler.printSQLException(e);

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                if (resultSet != null) resultSet.close();
                if (preparedStatement != null) preparedStatement.close();
                if (con != null) con.close();
            } catch (SQLException e) {
                SQLExceptionHandler.printSQLException(e);
            }
        }
        return null;
    }

    public List<Visitor> retrieveVisitorsBySiteId(String siteId) {
        if (siteId == null) return null;
        List<Visitor> visitors = new ArrayList<>();
        try {
            con = ConnectionPool.getInstance().getConnection();
            String query = "SELECT id, ip_address, user_agent, time, latitude, longitude FROM visitor WHERE site_id=?";
            preparedStatement = con.prepareStatement(query);
            preparedStatement.setString(1, siteId);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String ipAddress = resultSet.getString("ip_address");
                String userAgent = resultSet.getString("user_agent");
                Long time = resultSet.getLong("time");
                double latitude = resultSet.getDouble("latitude");
                double longitude = resultSet.getDouble("longitude");

                Visitor visitor = new Visitor(id, siteId, ipAddress, userAgent, time, latitude, longitude);
                visitors.add(visitor);

            }
            return visitors;
        } catch (SQLException e) {
            SQLExceptionHandler.printSQLException(e);

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                if (resultSet != null) resultSet.close();
                if (preparedStatement != null) preparedStatement.close();
                if (con != null) con.close();
            } catch (SQLException e) {
                SQLExceptionHandler.printSQLException(e);
            }
        }
        return null;
    }

    public Visitor retrieveVisitor(String siteId, String ipAddress) {
        if (siteId == null) return null;
        try {
            con = ConnectionPool.getInstance().getConnection();
            String query = "SELECT id, user_agent, time, latitude, longitude FROM visitor WHERE site_id=? AND ip_address=?";
            preparedStatement = con.prepareStatement(query);
            preparedStatement.setString(1, siteId);
            preparedStatement.setString(2, ipAddress);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                int id = resultSet.getInt("id");
                String userAgent = resultSet.getString("user_agent");
                Long time = resultSet.getLong("time");
                double latitude = resultSet.getDouble("latitude");
                double longitude = resultSet.getDouble("longitude");

                return (new Visitor(id, siteId, ipAddress, userAgent, time, latitude, longitude));
            }
        } catch (SQLException e) {
            SQLExceptionHandler.printSQLException(e);

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                if (resultSet != null) resultSet.close();
                if (preparedStatement != null) preparedStatement.close();
                if (con != null) con.close();
            } catch (SQLException e) {
                SQLExceptionHandler.printSQLException(e);
            }
        }
        return null;
    }

    public List<Visitor> retrieveAllVisitors() {

        List<Visitor> visitors = new ArrayList<>();
        try {
            con = ConnectionPool.getInstance().getConnection();
            String query = "SELECT id, site_id, ip_address, user_agent, time, latitude, longitude FROM visitor";
            statement = con.createStatement();
            resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String siteId = resultSet.getString("site_id");
                String ipAddress = resultSet.getString("ip_address");
                String userAgent = resultSet.getString("user_agent");
                Long time = resultSet.getLong("time");
                double latitude = resultSet.getDouble("latitude");
                double longitude = resultSet.getDouble("longitude");

                Visitor visitor = new Visitor(id, siteId, ipAddress, userAgent, time, latitude, longitude);
                visitors.add(visitor);

            }
            return visitors;
        } catch (SQLException e) {
            SQLExceptionHandler.printSQLException(e);

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                if (resultSet != null) resultSet.close();
                if (statement != null) statement.close();
                if (con != null) con.close();
            } catch (SQLException e) {
                SQLExceptionHandler.printSQLException(e);
            }
        }
        return null;
    }

    //    updates visitor on the basis of site_id and ip_address
    public boolean updateVisitor(Visitor visitor) {
        if (visitor == null) return false;
        try {
            con = ConnectionPool.getInstance().getConnection();
            String query = "UPDATE visitor SET user_agent=?, time=?, latitude=?, longitude=? WHERE site_id=? AND ip_address=?";
            preparedStatement = con.prepareStatement(query);
            preparedStatement.setString(1, visitor.getUserAgent());
            preparedStatement.setLong(2, visitor.getTime());
            preparedStatement.setDouble(3, visitor.getLatitude());
            preparedStatement.setDouble(4, visitor.getLongitude());
            preparedStatement.setString(5, visitor.getSiteId());
            preparedStatement.setString(6, visitor.getIpAddress());

            if (preparedStatement.executeUpdate() >= 1) return true;

        } catch (SQLException e) {
            SQLExceptionHandler.printSQLException(e);

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                if (preparedStatement != null) preparedStatement.close();
                if (con != null) con.close();
            } catch (SQLException e) {
                SQLExceptionHandler.printSQLException(e);
            }
        }
        return false;
    }

    public boolean deleteVisitor(Visitor visitor) {
        if (visitor == null) return false;
        try {
            con = ConnectionPool.getInstance().getConnection();

            String query = "DELETE FROM visitor WHERE site_id=? AND ip_address=?";

            preparedStatement = con.prepareStatement(query);
            preparedStatement.setString(1, visitor.getSiteId());
            preparedStatement.setString(2, visitor.getIpAddress());

            if (preparedStatement.executeUpdate() > 0) return true;

        } catch (SQLException e) {
            SQLExceptionHandler.printSQLException(e);

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                if (preparedStatement != null) preparedStatement.close();
                if (con != null) con.close();
            } catch (SQLException e) {
                SQLExceptionHandler.printSQLException(e);
            }
        }
        return false;
    }

}
