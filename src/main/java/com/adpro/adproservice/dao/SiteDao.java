package com.adpro.adproservice.dao;

import com.adpro.adproservice.dbconnection.ConnectionPool;
import com.adpro.adproservice.exceptionhandling.SQLExceptionHandler;
import com.adpro.adproservice.model.Site;
import com.adpro.adproservice.model.User;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SiteDao {
    private Connection con = null;
    private PreparedStatement preparedStatement = null;
    private Statement statement = null;
    private ResultSet resultSet = null;

    public boolean createSite(Site site) {
        if (site == null) return false;

        try {
            con = ConnectionPool.getInstance().getConnection();
            String query = "INSERT INTO site VALUES (?, ?, ?)";
            preparedStatement = con.prepareStatement(query);
            preparedStatement.setString(1, UUID.randomUUID().toString());
            preparedStatement.setString(2, site.getSiteUrl());
            preparedStatement.setInt(3, site.getUserId());
            if (preparedStatement.executeUpdate() == 1) return true;
        } catch (SQLException e) {
            SQLExceptionHandler.printSQLException(e);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) preparedStatement.close();
                if (con != null) con.close();
            } catch (SQLException e) {
                SQLExceptionHandler.printSQLException(e);
            }
        }
        return false;
    }

    public List<Site> retrieveAllSites(int userId) {
        if (userId < 1) return null;

        try {
            con = ConnectionPool.getInstance().getConnection();
            con.setAutoCommit(false);

            String query = "SELECT id, site_url FROM site WHERE user_id = ?";
            preparedStatement = con.prepareStatement(query);
            preparedStatement.setInt(1, userId);
            resultSet = preparedStatement.executeQuery();

            List<Site> sites = new ArrayList<>();
            while (resultSet.next()) {

                String siteId = resultSet.getString("id");
                String siteUrl = resultSet.getString("site_url");
                sites.add(new Site(siteId, siteUrl, userId));
            }
            return sites;
        } catch (SQLException e) {
            SQLExceptionHandler.printSQLException(e);

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                if (resultSet != null) resultSet.close();
                if (preparedStatement != null) preparedStatement.close();
                if (con != null) con.close();
            } catch (SQLException e) {
                SQLExceptionHandler.printSQLException(e);
            }
        }
        return null;
    }

    public List<Site> retrieveAllSites() {
        List<Site> sites = new ArrayList<>();
        try {
            con = ConnectionPool.getInstance().getConnection();
            String query = "SELECT id, site_url, user_id FROM site";
            statement = con.createStatement();
            resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                String id = resultSet.getString("id");
                String siteUrl = resultSet.getString("site_url");
                int userId = resultSet.getInt("user_id");
                sites.add(new Site(id, siteUrl, userId));
            }
            return sites;
        } catch (SQLException e) {
            SQLExceptionHandler.printSQLException(e);
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                if (resultSet != null) resultSet.close();
                if (statement != null) statement.close();
                if (con != null) con.close();
            } catch (SQLException e) {
                SQLExceptionHandler.printSQLException(e);
            }
        }
        return null;
    }

    public boolean updateSite(Site site) {
        if (site == null) return false;
        try {
            con = ConnectionPool.getInstance().getConnection();

            String query = "UPDATE site SET site_url = ? WHERE id=?";
            preparedStatement = con.prepareStatement(query);
            preparedStatement.setString(1, site.getSiteUrl());
            preparedStatement.setString(2, site.getId());

            if (preparedStatement.executeUpdate() == 1) return true;

        } catch (SQLException e) {
            SQLExceptionHandler.printSQLException(e);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) preparedStatement.close();
                if (con != null) con.close();
            } catch (SQLException e) {
                SQLExceptionHandler.printSQLException(e);
            }
        }
        return false;
    }

    public boolean deleteSite(String id) {
        if (id == null) return false;

        try {
            con = ConnectionPool.getInstance().getConnection();

            String query = "DELETE FROM site WHERE id = ?";

            preparedStatement = con.prepareStatement(query);
            preparedStatement.setString(1, id);

            if (preparedStatement.executeUpdate() > 0) return true;
        } catch (SQLException e) {
            SQLExceptionHandler.printSQLException(e);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) preparedStatement.close();
                if (con != null) con.close();
            } catch (SQLException e) {
                SQLExceptionHandler.printSQLException(e);
            }
        }
        return false;
    }

}

