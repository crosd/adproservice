package com.adpro.adproservice.dao;

import com.adpro.adproservice.dbconnection.ConnectionPool;
import com.adpro.adproservice.exceptionhandling.SQLExceptionHandler;
import com.adpro.adproservice.model.AuthToken;
import com.adpro.adproservice.model.User;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Base64;
import java.util.Date;
import java.util.Random;


public class SessionDao {
    private PreparedStatement sessionpreparedStatement = null;
    private Connection con = null;
    private ResultSet resultSet = null;
    private AuthToken authToken = null;


    public AuthToken sessionStart(User user) {

        if (user == null) return null;
        try {
            con = ConnectionPool.getInstance().getConnection();
            con.setAutoCommit(false);

            authToken = this.generateAuthToken(user); // returns encoded token
            sessionpreparedStatement = con.prepareStatement("INSERT INTO session VALUES(?,?,?,?)");
            sessionpreparedStatement.setString(1,authToken.getToken());
            sessionpreparedStatement.setString(2,authToken.getEmailId());
            sessionpreparedStatement.setLong(3,authToken.getStart());
            sessionpreparedStatement.setLong(4,authToken.getLastActive());

            int sessionRowsAffected = sessionpreparedStatement.executeUpdate();

            con.commit();
            if (sessionRowsAffected == 1)
                return authToken;
        } catch (SQLException e) {
            SQLExceptionHandler.printSQLException(e);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean sessionEnd(AuthToken authToken) {
        if (authToken == null) return false;
        try {
            con = ConnectionPool.getInstance().getConnection();
            con.setAutoCommit(false);
            sessionpreparedStatement = con.prepareStatement("DELETE FROM session WHERE  email_id= ?");
            sessionpreparedStatement.setString(1,authToken.getEmailId());

           if (sessionpreparedStatement.executeUpdate()> 0) {
               con.commit();
               return true;
            }
        } catch (SQLException e) {
            SQLExceptionHandler.printSQLException(e);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public AuthToken generateAuthToken(User user){

        String encodedToken= this.encodeAuthToken(user.getEmailId());
        // Instantiate a Date object
        long start = new Date().getTime();
        long lastActive=new Date().getTime();
        return new AuthToken(encodedToken,user.getEmailId(),start,lastActive);
    }

    public String encodeAuthToken(String emailId) {
        Random r = new Random();
        int random = r.nextInt();
        String str = emailId + random;
        // encode data on your side using BASE64
        String encodedtoken = Base64.getEncoder().encodeToString(str.getBytes());
        return encodedtoken;

    }

    public String decodeAuthToken(AuthToken authToken){

        // decode encoded token
        byte [] decodedtoken = Base64.getDecoder().decode(authToken.getToken());
        String decoded= new String(decodedtoken);
        return decoded;
    }

    public AuthToken getAuthToken(String email) {
        try {
            con = ConnectionPool.getInstance().getConnection();
            con.setAutoCommit(false);

            String query = "SELECT * FROM session WHERE email_id = ?";
            sessionpreparedStatement = con.prepareStatement(query);
            sessionpreparedStatement.setString(1,email);
            resultSet = sessionpreparedStatement.executeQuery();

            if (resultSet.next()) {
                String encodedtoken = resultSet.getString("token");
                String emailId = resultSet.getString("email_id");
                long start=resultSet.getLong("start");
                long lastActive=resultSet.getLong("lastActive");
                return new AuthToken(encodedtoken, emailId,start,lastActive);

            }
        } catch (SQLException e) {
            SQLExceptionHandler.printSQLException(e);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean setLastActive(AuthToken authToken){

        try {
            con = ConnectionPool.getInstance().getConnection();
            con.setAutoCommit(false);
            sessionpreparedStatement = con.prepareStatement("UPDATE SESSION " +
                    "SET lastActive=?"+
                    "WHERE email=?");
            sessionpreparedStatement.setLong(1,authToken.getLastActive());
            sessionpreparedStatement.setString(2,authToken.getEmailId());

            int sessionRowsAffected = sessionpreparedStatement.executeUpdate();

            con.commit();
            if(sessionRowsAffected==1){
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }
        return  false;
    }





    public void sesssionFlush() { //flushing inactive sessions

        try {
            con = ConnectionPool.getInstance().getConnection();
            con.setAutoCommit(false);
            long currentTime = new Date().getTime();
            System.out.println(currentTime);
            sessionpreparedStatement = con.prepareStatement("DELETE FROM session WHERE ("+currentTime+"- CAST(lastActive AS long)>300000");
            if (sessionpreparedStatement.executeUpdate() > 0) {
                con.commit();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }
      //  return false;
    }

}